const std = @import("std");

pub fn writeDummyLibFile(fname: []const u8, dname: []const u8, libname: []const u8, functions: anytype) !void {
    var lib_file = std.fs.cwd().createFile(fname, .{}) catch |err| {
        fatal("unable to open '{s}': {s}", .{ fname, @errorName(err) });
    };
    var def_file = std.fs.cwd().createFile(dname, .{}) catch |err| {
        fatal("unable to open '{s}': {s}", .{ dname, @errorName(err) });
    };
    defer lib_file.close();
    defer def_file.close();

    // Create buffered writers for performance
    var lib_buf = std.io.bufferedWriter(lib_file.writer());
    var def_buf = std.io.bufferedWriter(def_file.writer());
    var lib_writer = lib_buf.writer();
    var def_writer = def_buf.writer();
    // Write def file header
    try def_writer.print(
        \\LIBRARY {s}
        \\EXPORTS
        \\
    , .{libname});

    for (functions) |func| {
        try writeAnonymousFunction(lib_writer, def_writer, func);
    }

    try lib_buf.flush();
    try def_buf.flush();
    return std.process.cleanExit();
}

fn writeAnonymousFunction(lib_file: anytype, def_file: anytype, args: anytype) !void {
    try lib_file.print("export fn f{d}(", .{args[0]});
    var ret_bytes = args[1];
    var current_arg_length: u32 = 8;
    while (ret_bytes > 0) {
        if (ret_bytes < current_arg_length) {
            current_arg_length /= 2;
        }
        ret_bytes -= current_arg_length;
        try lib_file.print("_: i{d}", .{current_arg_length * 8});
        if (ret_bytes > current_arg_length - current_arg_length) {
            _ = try lib_file.write(", ");
        }
    }
    try lib_file.print(") callconv(.Stdcall) i32 {{\n    return {d};\n}}\n\n", .{args[2]});
    try def_file.print("      f{d} @{d} NONAME\n", .{ args[0], args[0] });
}

fn fatal(comptime format: []const u8, args: anytype) noreturn {
    std.debug.print(format, args);
    std.process.exit(1);
}
