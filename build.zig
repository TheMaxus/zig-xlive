const std = @import("std");

// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) !void {
    // This DLL is only used in x86 programs
    const target = .{
        .cpu_arch = .x86,
        .os_tag = .windows,
        .abi = .gnu,
    };

    // ReleaseSmall to reduce size
    const optimize = .ReleaseSmall;

    const lib = b.addObject(.{
        .name = "xlive",
        // In this case the main source file is merely a path, however, in more
        // complicated build scripts, this could be a generated file.
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });
    lib.strip = true;

    // Create DLL file out of .lib file
    // This step is only needed because Zig doesn't let you
    // specify the def file otherwise
    const dll = b.step("dll", "Create DLL file");
    const lib_target = try lib.target.zigTriple(b.allocator);
    var zig_cc_args = std.ArrayList([]const u8).init(b.allocator);
    try zig_cc_args.appendSlice(&[_][]const u8{
        "zig",
        "cc",
        "-Os",
        "-s",
        "-shared",
        "-dynamic",
        "-target",
        lib_target,
        "src/xlive.def",
    });
    const run_zig_cc = b.addSystemCommand(zig_cc_args.items);
    run_zig_cc.addArtifactArg(lib);
    run_zig_cc.addArg("-o");
    var dll_file = run_zig_cc.addOutputFileArg("xlive.dll");
    run_zig_cc.step.dependOn(&lib.step);
    var install_dll = b.addInstallLibFile(dll_file, "xlive.dll");
    install_dll.step.dependOn(&run_zig_cc.step);
    dll.dependOn(&install_dll.step);
    b.default_step.dependOn(dll);
}
